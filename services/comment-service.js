const cote = require('cote'),
    mongoose = require('mongoose');

mongoose.connect('mongodb://db/test');

const CommentModel = mongoose.model('Comment', new mongoose.Schema({
  postId: 'ObjectId',
  content: 'string',
  date: {
    type: Date,
  }
}));


var commentResponder = new cote.Responder({
  name: 'comment responder',
  namespace: 'comment',
  respondsTo: ['add', 'get', 'getByPost']
});

commentResponder.on('*', console.log);

commentResponder.on('add', (req) => new CommentModel({...req.comment, date: Date.now()}).save());

commentResponder.on('get', (req) => CommentModel.findById(req._id));

commentResponder.on('getByPost', (req) => CommentModel.find({postId: req.postId}));

