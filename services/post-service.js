const cote = require('cote'),
    mongoose = require('mongoose');

mongoose.connect('mongodb://db/test');

const PostModel = mongoose.model('Post', new mongoose.Schema({
  title: 'string',
  content: 'string',
  date: {
    type: Date,
  }
}));


var postResponder = new cote.Responder({
  name: 'post responder',
  namespace: 'post',
  respondsTo: ['get', 'list']
});

postResponder.on('*', console.log);

postResponder.on('add', (req) => new PostModel({...req.post, date: Date.now()}).save());

postResponder.on('get', (req) => PostModel.findById(req._id));

postResponder.on('list', (req) => PostModel.find());

