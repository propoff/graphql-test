FROM node:alpine

WORKDIR /src
ADD package.json .
RUN yarn install

ADD . .

