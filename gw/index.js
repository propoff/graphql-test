const express = require('express');
const {graphqlExpress, graphiqlExpress} = require('graphql-server-express');
const {makeExecutableSchema} = require('graphql-tools');
const cote = require('cote');

const typeDefs = [`
      type Query {
        post(_id: String): Post
        posts: [Post]
        comment(_id: String): Comment
      }
      type Post {
        _id: String
        title: String
        content: String
        date: String
        comments: [Comment]
      }
      type Comment {
        _id: String
        postId: String
        date: String
        content: String
        post: Post
      }
      type Mutation {
        createPost(title: String, content: String): Post
        createComment(postId: String, content: String): Comment
      }
      schema {
        query: Query
        mutation: Mutation
      }
    `];

var postRequester = new cote.Requester({
    name: 'post requester',
    namespace: 'post'
});


var commentRequester = new cote.Requester({
    name: 'comment requester',
    namespace: 'comment'
});



const resolvers = {
  Query: {
    post: (root, {_id}) => {
      return postRequester.send({type: 'get', _id});
    },
    posts: () => {
      return postRequester.send({type: 'list'});
    },
    comment: (root, {_id}) => {
      return commentRequester.send({type: 'get', _id});
    },
  },
  Post: {
    comments: ({_id}) => {
      return commentRequester.send({type: 'getByPost', postId: _id});
    }
  },
  Comment: {
    post: async ({postId}) => {
      return postRequester.send({type: 'get', postId});
  
      // return prepare(await Posts.findOne(ObjectId(postId)))
    }
  },
  Mutation: {
    createPost: (root, post, context, info) => postRequester.send({type: 'add', post}),
    createComment: (root, comment) => commentRequester.send({type: 'add', comment})
  }
};

const schema = makeExecutableSchema({
  typeDefs,
  resolvers
});

const app = express();

app.use(require('cors')());

app.use('/graphql', require('body-parser').json(), graphqlExpress({schema}));


app.use('/graphiql', graphiqlExpress({
  endpointURL: '/graphql'
}));

app.listen(4000, () => {
  console.log(`Visit http://localhost:4000/graphiql`);
});
